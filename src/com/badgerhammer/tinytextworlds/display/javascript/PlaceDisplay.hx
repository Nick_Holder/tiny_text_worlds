 /*  
  * Copyright (c) 2012 Nick Holder - hello@nickholder.co.uk
  * 
  * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the   * "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
  * 
  * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
  * 
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
  * 
*/

package com.badgerhammer.tinytextworlds.display.javascript;

import com.badgerhammer.tinytextworlds.TextBlock;
import com.badgerhammer.tinytextworlds.actions.ActionInfo;




#if haxe3
import js.html.Element;
import js.html.Event;
import js.Browser;
import js.html.Document;
#else
import js.Dom;
#end

import js.Lib;


/**
 * ...
 * @author Nick Holder
 */

class PlaceDisplay 
{
	
	#if haxe3
	private var _ttwPlaceDiv:Element;
	private var _interactiveDomElements:Array<Element>;
	#else
	private var _ttwPlaceDiv:HtmlDom;
	private var _interactiveDomElements:Array<HtmlDom>;
	
	#end
	
	var _document:Document;
	
	public var _displayActionFunction:Array<ActionInfo> -> String -> String -> Void;
	public var _placeTextBlocks:Array<TextBlock>;

	
	public function new( displayActionFunction:Array<ActionInfo> -> String -> String -> Void ) 
	{
		#if haxe3
		_document = Browser.document;
		#else
		_document = Lib.document;
		#end
		
		_displayActionFunction = displayActionFunction;
		
		// List interactive DOM elements to remove click listeners later
		#if haxe3
		_interactiveDomElements = new Array<Element>();
		#else
		_interactiveDomElements = new Array<HtmlDom>();
		#end
		
		// Find the div tag to write to
		_ttwPlaceDiv = _document.getElementById( "TTWPlace" );
		
	}

	// Display all the place TextBlocks
	public function displayPlace( placeTextBlocks:Array<TextBlock> ):Void
	{
		clear();
		
		// Store placeTextBlocks
		_placeTextBlocks = placeTextBlocks;
		
		var firstSentence:Bool = true;
		var createdTextBlocks:Int = 0;
		
		// iterate through all the TextBlocks in placeTextBlocks
		for ( textBlock in placeTextBlocks )
		{
			
			#if haxe3
			var element:Element;
			#else
			var element:HtmlDom;
			#end
			
			// If the TextBlock has an Id
			// It is a place or an object
			
			
			if ( textBlock.id != "" )
			{
				// Does the textblock have any actions?
				if ( textBlock.actions.length > 0 )
				{
					// Make a new element for this place/object
					element = _document.createElement("a");
					// Has actions so add an onclick
					element.onclick = interactiveDomElement_onClick;
					
					// Keep track of all the elements with listeners
					_interactiveDomElements.push( element );
					
					element.className = textBlock.type;
					element.className += "-has-actions interactive";
					
					element.setAttribute( "href" , "#TTWActions" );
				}
				else
				{
					// Make a new element for this place/object
					element = _document.createElement("span");
				
					// Add -no-actions to the classname for CSS
					element.className = textBlock.type;
					element.className += "-no-actions interactive";
				}
				
				element.id = textBlock.id;
				element.innerHTML = textBlock.text;
			}
			else
			// This is a standard text block
			{
				element = _document.createElement("span");
				element.className = textBlock.type;
				
				
				
				if ( firstSentence )
				{
					element.innerHTML = "<span class=\"first-letter\">" + textBlock.text.charAt( 0 ) + "</span>";
					element.innerHTML += textBlock.text.substr( 1 );
					
					firstSentence = false;
				}
				else
				{
					element.innerHTML = textBlock.text;
				}
			}
			
			// Has this text changed since the last time the user saw it?
			if ( textBlock.hasChanged )
			{
				element.className += " has-changed";
			}
			
			createdTextBlocks ++;
			
			if ( createdTextBlocks == 3 )
			{
				createdTextBlocks = 0;
				element.innerHTML += " ";
			}
			
			// Add to the text in the place div
			_ttwPlaceDiv.appendChild( element );	
		}	

	}

	
	
	// Reset the classes of clicked buttons
	private function resetClickedButtons():Void
	{
		// Reset all object buttons to not clicked state
		for ( element in _interactiveDomElements  )
		{
			var classes:Array<String> = element.className.split(" ");
			
			element.className = "";
			
			// Iterate through classNames re-adding but excluding object-clicked
			for ( className in classes )
			{
				if ( className == "object-clicked" )
				{
					continue;
				}
				
				element.className += className +" ";
			}
			 
		}
	}
	
	
	
	// Click event handler
	private function interactiveDomElement_onClick( e : Event ):Void
	{	
		resetClickedButtons();
		
		// Set this action button as clicked
		#if haxe3
		// Casting to Element failed in Safari so using Dynamic
		var clickedElement:Dynamic = e.target;
		clickedElement.className += " object-clicked";
		#else
		e.target.className += " object-clicked";
		#end
		
		
		
		
		var actionsArray:Array<ActionInfo>  =  new Array<ActionInfo> ( );
		var textBlockId:String = "";
		var textBlockName:String = "";
		
		// Match event id to a textBlock id
		for ( textBlock in _placeTextBlocks )
		{
			#if haxe3
			if ( textBlock.id == clickedElement.id )
			#else
			if ( textBlock.id == e.target.id )
			#end
			{
				// Get the TextBlock's actions
				actionsArray = textBlock.actions;
				textBlockId = textBlock.id;
				textBlockName = textBlock.text ;
				break;
			}
		}
		
		// display the actions, included the textblockID so we can callback to GameLogic for action resolution
		_displayActionFunction( actionsArray , textBlockId , textBlockName);
	}
	
	
	
	// Clear the div and all listeners on objects in it
	public function clear():Void
	{
		removeListeners();
		_ttwPlaceDiv.innerHTML = "";
	}
	
	
	
	// Remove listeners from all interactive DOM elements
	private function removeListeners():Void
	{
		while ( _interactiveDomElements.length > 0 )
		{
			_interactiveDomElements.shift().onclick = null;
		}
	}
	
	
	/* OLD EVENT CODE
	// Add a listener to an interactive DOM element
	private function addListener( domElement:HtmlDom ):Void
	{
		untyped
		{    
			 if ( js.Lib.isIE )
			{
				domElement.attachEvent( "click", interactiveDomElement_onClick, false );
			} else {
				domElement.addEventListener( "click", interactiveDomElement_onClick, false );
			}
		}
	}
	
	// Remove listeners from all interactive DOM elements
	private function removeListeners():Void
	{
			
			untyped
			{
				if ( js.Lib.isIE )
				{
					interactiveDomElements.shift().detachEvent( "click", interactiveDomElement_onClick );
				} else {
					interactiveDomElements[0].removeEventListener( "click", interactiveDomElement_onClick );
					interactiveDomElements.shift();
				}
			}
			
		}
		
	}
	*/
}