 /*  
  * Copyright (c) 2012 Nick Holder - hello@nickholder.co.uk
  * 
  * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the   * "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
  * 
  * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
  * 
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
  * 
*/

package com.badgerhammer.tinytextworlds;

import com.badgerhammer.xml.BHXmlTools;
import msignal.Signal;

#if !flash
import com.badgerhammer.xml.general.XmlLoader;
#elseif flash
import com.badgerhammer.xml.flash.XmlLoader;
#end

/**
 * Holds and interacts with the world Xml file
 * @author Nick Holder
 */

class WorldXml 
{	
	private var _xmlLoader:XmlLoader;
	private var _xml:Xml;
	
	public var worldLoadCompleted:Signal0;
	
	public function new() 
	{
		worldLoadCompleted = new Signal0();
	}
	
	public function report():Xml
	{
		return _xml;
	}
	
	// Start a load and store load complete callback
	public function load( url:String ):Void
	{
		
		_xmlLoader = new XmlLoader();
		_xmlLoader.loadXML( url , loadComplete);
	}
	
	
	
	// Load has been completed with no errors
	private function loadComplete( xml:Xml ):Void
	{
		_xml =  xml;
		_xmlLoader = null;
		
		worldLoadCompleted.dispatch();
	}

	
	
	// Get the location of the player
	public function getPlayerLocation():String
	{
		return _xml.elements().next().elementsNamed("player").next().get( "location" );
	}
	
	
	
	// Gety the players xml
	public function getPlayer():Xml
	{
		return _xml.elements().next().elementsNamed("player").next();
	}
	
	
	
	// Get a place by it's ID
	public function getPlace( placeId:String ):Xml
	{
		var placesXmlIterator:Iterator<Xml> =  _xml.elements().next().elementsNamed("places").next().elementsNamed( "place" );
		
		return BHXmlTools.findNodeByAttributeValue( placesXmlIterator , "id" , placeId );
	}
	
	
	
	// Get a object by it's ID
	public function getObject( objectId:String ):Xml
	{
		var objectsXmlIterator:Iterator<Xml> =  _xml.elements().next().elementsNamed("objects").next().elementsNamed( "object" );
		
		var xml:Xml = BHXmlTools.findNodeByAttributeValue( objectsXmlIterator , "id" , objectId );
		
		return xml;
	}
	
	
	
	// Get an objects action by the object and action ID
	public function getObjectAction( objectId:String , actionId:String ):Xml
	{
		var objectsXmlIterator:Iterator<Xml> =  _xml.elements().next().elementsNamed("objects").next().elementsNamed( "object" );
		
		var actionsXmlIterator:Iterator<Xml> = BHXmlTools.findNodeByAttributeValue( objectsXmlIterator , "id" , objectId ).elementsNamed( "action" );
		return BHXmlTools.findNodeByAttributeValue( actionsXmlIterator , "id" , actionId );
	}
	
	
	
	// The default cancel action, contains default text for cancel actions ( for localisation )
	public function getCancelText():String
	{
		return _xml.elements().next().elementsNamed("cancel").next().get( "defaultText" );
	}
	
	
	
	// The default action prompt text, used for localisation e.g. Choose and action for: __objectName__
	public function getActionPromptText():String
	{
		
		return _xml.elements().next().elementsNamed("actionPrompt").next().get("defaultText");
	}
	
	
	
	// The object which contains the startup action
	public function getStartUpObjectId():String
	{
		return _xml.elements().next().elementsNamed("startUpAction").next().get("objectId");
	}
	
	
	
	// The startup action id
	public function getStartUpActionId():String
	{
		return _xml.elements().next().elementsNamed("startUpAction").next().get("actionId");
	}
	
	public function isUsingGlobalObjects():Bool 
	{
		return _xml.elements().next().elementsNamed("useGlobalObjects").next().get("flag") == "true";
	}
}