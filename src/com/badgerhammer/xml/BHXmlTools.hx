 /*  
  * Copyright (c) 2012 Nick Holder - hello@nickholder.co.uk
  * 
  * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the   * "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
  * 
  * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
  * 
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
  * 
*/

package com.badgerhammer.xml;
import com.badgerhammer.string.BHStringTools;

/**
 * ...
 * @author Nick Holder
 */

class BHXmlTools 
{

	// Find the first node in iterator:Iterator<Xml> with an attributeName:String that matches value:String
	public static function findNodeByAttributeValue( iterator:Iterator<Xml> , attributeName:String ,  attributeValue:String ):Xml
	{
		var xml:Xml =  null;
		var foundMatch:Bool = false;
		
		
		while ( iterator.hasNext() )
		{
			xml = iterator.next();
			
			if ( xml.get( attributeName ) == attributeValue )
			{
				foundMatch = true;
				break;
			}
		}
		
		if ( !foundMatch ) trace( "Script Error! No match found:" + " " + attributeName + " = " + attributeValue );
		
		return xml;
	}
	
	public static function doArithmeticOnAttribute ( targetNode:Xml, targetAttribute:String , arithmeticMode:String, number:String) 
	{
		var change:Float = Std.parseFloat( number );
		if ( arithmeticMode == "-" )
		{
			change *= -1;
		}
		
		var currentValue:String = targetNode.get( targetAttribute );
		
		if ( BHStringTools.startsWithNumber( currentValue ) )
		{
			targetNode.set( targetAttribute , Std.string( Std.parseFloat( currentValue ) + change ) );
		}
		else
		{
			trace( "No Number found" );
		}
	}
	
	
	
}