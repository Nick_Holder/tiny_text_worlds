 /*  
  * Copyright (c) 2012 Nick Holder - hello@nickholder.co.uk
  * 
  * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the   * "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
  * 
  * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
  * 
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
  * 
*/

package com.badgerhammer.tinytextworlds;

import com.badgerhammer.tinytextworlds.WorldXml;
import com.badgerhammer.xml.BHXmlTools;
import com.badgerhammer.tinytextworlds.actions.ActionInfo;

/**
 * ...
 * @author Nick Holder
 */

class TTWPlace 
{
	
	private var _xml:Xml;
	private var _objects:Array<TTWObject>;
	private var _worldXmlRef:WorldXml;
	
	
	
	public function new( worldXml:WorldXml , currentPlayerLocation:String ) 
	{
		_worldXmlRef = worldXml;
		
		// Store params for later use
		_xml = worldXml.getPlace( currentPlayerLocation );
		
		// Objects
		makeObjects( worldXml );
		addGlobalObjects( worldXml );
	}
	
	
	
	// Make all the objects
	public function makeObjects( worldXml:WorldXml):Void
	{
		// Make array for TTWObjects
		_objects = new Array<TTWObject>();
		
		// Iterate through Xml making objects
		var objectsXmlIterator:Iterator<Xml> =  _xml.elementsNamed("object");
		
		while ( objectsXmlIterator.hasNext() )
		{
			_objects.push( new TTWObject( _worldXmlRef , worldXml.getObject( objectsXmlIterator.next().get("id") ) ) );
		}
	}
	
	// Add the globally visible objects
	public function addGlobalObjects( worldXml:WorldXml):Void
	{
		if ( !worldXml.isUsingGlobalObjects() ) return;
		
		// Iterate through Xml making objects
		var objectsXmlIterator:Iterator<Xml> =  worldXml.getPlace("global").elementsNamed("object");
		
		while ( objectsXmlIterator.hasNext() )
		{
			_objects.push( new TTWObject( _worldXmlRef , worldXml.getObject( objectsXmlIterator.next().get("id") ) ) );
		}
	}
	
	// Get the text for the place including all the text for contained objects
	public function getTextBlocks():Array<TextBlock>
	{
		var textBlocks = new Array<TextBlock>();

		// Iterate through objects getting their TextBlocks, then adding them to the textBlocks Array to return
		for ( object in _objects)
		{
			// Skip this object if it's not currently visible
			if ( !object.objectVisible() )
			{
				continue;
			}
			
			var objectTextBlocks:Array<TextBlock> = object.getTextBlocks() ;
			textBlocks.push( objectTextBlocks[0] );
			textBlocks.push( objectTextBlocks[1] );
			textBlocks.push( objectTextBlocks[2] );
		}
		
		return textBlocks;
	}
	
	
}