(function () { "use strict";
var $estr = function() { return js.Boot.__string_rec(this,''); };
function $extend(from, fields) {
	function inherit() {}; inherit.prototype = from; var proto = new inherit();
	for (var name in fields) proto[name] = fields[name];
	if( fields.toString !== Object.prototype.toString ) proto.toString = fields.toString;
	return proto;
}
var EReg = function(r,opt) {
	opt = opt.split("u").join("");
	this.r = new RegExp(r,opt);
};
EReg.__name__ = true;
EReg.prototype = {
	replace: function(s,by) {
		return s.replace(this.r,by);
	}
}
var HxOverrides = function() { }
HxOverrides.__name__ = true;
HxOverrides.cca = function(s,index) {
	var x = s.charCodeAt(index);
	if(x != x) return undefined;
	return x;
}
HxOverrides.substr = function(s,pos,len) {
	if(pos != null && pos != 0 && len != null && len < 0) return "";
	if(len == null) len = s.length;
	if(pos < 0) {
		pos = s.length + pos;
		if(pos < 0) pos = 0;
	} else if(len < 0) len = s.length + len - pos;
	return s.substr(pos,len);
}
HxOverrides.remove = function(a,obj) {
	var i = 0;
	var l = a.length;
	while(i < l) {
		if(a[i] == obj) {
			a.splice(i,1);
			return true;
		}
		i++;
	}
	return false;
}
HxOverrides.iter = function(a) {
	return { cur : 0, arr : a, hasNext : function() {
		return this.cur < this.arr.length;
	}, next : function() {
		return this.arr[this.cur++];
	}};
}
var Main = function() {
	haxe.Log.trace = $bind(this,this.divTrace);
	this._gameLogic = new com.badgerhammer.tinytextworlds.GameLogic(js.Browser.window.fileName);
};
Main.__name__ = true;
Main.main = function() {
	new Main();
}
Main.prototype = {
	divTrace: function(v,i) {
		js.Browser.document.getElementById("haxe:trace").innerHTML += "<br>[" + i.fileName + " : " + i.lineNumber + "] " + i.methodName + "() - " + Std.string(v);
	}
}
var IMap = function() { }
IMap.__name__ = true;
var Reflect = function() { }
Reflect.__name__ = true;
Reflect.isFunction = function(f) {
	return typeof(f) == "function" && !(f.__name__ || f.__ename__);
}
Reflect.compareMethods = function(f1,f2) {
	if(f1 == f2) return true;
	if(!Reflect.isFunction(f1) || !Reflect.isFunction(f2)) return false;
	return f1.scope == f2.scope && f1.method == f2.method && f1.method != null;
}
var Std = function() { }
Std.__name__ = true;
Std.string = function(s) {
	return js.Boot.__string_rec(s,"");
}
Std.parseInt = function(x) {
	var v = parseInt(x,10);
	if(v == 0 && (HxOverrides.cca(x,1) == 120 || HxOverrides.cca(x,1) == 88)) v = parseInt(x);
	if(isNaN(v)) return null;
	return v;
}
Std.parseFloat = function(x) {
	return parseFloat(x);
}
var StringBuf = function() {
	this.b = "";
};
StringBuf.__name__ = true;
StringBuf.prototype = {
	addSub: function(s,pos,len) {
		this.b += len == null?HxOverrides.substr(s,pos,null):HxOverrides.substr(s,pos,len);
	}
}
var StringTools = function() { }
StringTools.__name__ = true;
StringTools.urlEncode = function(s) {
	return encodeURIComponent(s);
}
StringTools.htmlEscape = function(s,quotes) {
	s = s.split("&").join("&amp;").split("<").join("&lt;").split(">").join("&gt;");
	return quotes?s.split("\"").join("&quot;").split("'").join("&#039;"):s;
}
StringTools.htmlUnescape = function(s) {
	return s.split("&gt;").join(">").split("&lt;").join("<").split("&quot;").join("\"").split("&#039;").join("'").split("&amp;").join("&");
}
var XmlType = { __ename__ : true, __constructs__ : [] }
var Xml = function() {
};
Xml.__name__ = true;
Xml.parse = function(str) {
	return haxe.xml.Parser.parse(str);
}
Xml.createElement = function(name) {
	var r = new Xml();
	r.nodeType = Xml.Element;
	r._children = new Array();
	r._attributes = new haxe.ds.StringMap();
	r.set_nodeName(name);
	return r;
}
Xml.createPCData = function(data) {
	var r = new Xml();
	r.nodeType = Xml.PCData;
	r.set_nodeValue(data);
	return r;
}
Xml.createCData = function(data) {
	var r = new Xml();
	r.nodeType = Xml.CData;
	r.set_nodeValue(data);
	return r;
}
Xml.createComment = function(data) {
	var r = new Xml();
	r.nodeType = Xml.Comment;
	r.set_nodeValue(data);
	return r;
}
Xml.createDocType = function(data) {
	var r = new Xml();
	r.nodeType = Xml.DocType;
	r.set_nodeValue(data);
	return r;
}
Xml.createProcessingInstruction = function(data) {
	var r = new Xml();
	r.nodeType = Xml.ProcessingInstruction;
	r.set_nodeValue(data);
	return r;
}
Xml.createDocument = function() {
	var r = new Xml();
	r.nodeType = Xml.Document;
	r._children = new Array();
	return r;
}
Xml.prototype = {
	toString: function() {
		if(this.nodeType == Xml.PCData) return StringTools.htmlEscape(this._nodeValue);
		if(this.nodeType == Xml.CData) return "<![CDATA[" + this._nodeValue + "]]>";
		if(this.nodeType == Xml.Comment) return "<!--" + this._nodeValue + "-->";
		if(this.nodeType == Xml.DocType) return "<!DOCTYPE " + this._nodeValue + ">";
		if(this.nodeType == Xml.ProcessingInstruction) return "<?" + this._nodeValue + "?>";
		var s = new StringBuf();
		if(this.nodeType == Xml.Element) {
			s.b += "<";
			s.b += Std.string(this._nodeName);
			var $it0 = this._attributes.keys();
			while( $it0.hasNext() ) {
				var k = $it0.next();
				s.b += " ";
				s.b += Std.string(k);
				s.b += "=\"";
				s.b += Std.string(this._attributes.get(k));
				s.b += "\"";
			}
			if(this._children.length == 0) {
				s.b += "/>";
				return s.b;
			}
			s.b += ">";
		}
		var $it1 = this.iterator();
		while( $it1.hasNext() ) {
			var x = $it1.next();
			s.b += Std.string(x.toString());
		}
		if(this.nodeType == Xml.Element) {
			s.b += "</";
			s.b += Std.string(this._nodeName);
			s.b += ">";
		}
		return s.b;
	}
	,addChild: function(x) {
		if(this._children == null) throw "bad nodetype";
		if(x._parent != null) HxOverrides.remove(x._parent._children,x);
		x._parent = this;
		this._children.push(x);
	}
	,firstChild: function() {
		if(this._children == null) throw "bad nodetype";
		return this._children[0];
	}
	,elementsNamed: function(name) {
		if(this._children == null) throw "bad nodetype";
		return { cur : 0, x : this._children, hasNext : function() {
			var k = this.cur;
			var l = this.x.length;
			while(k < l) {
				var n = this.x[k];
				if(n.nodeType == Xml.Element && n._nodeName == name) break;
				k++;
			}
			this.cur = k;
			return k < l;
		}, next : function() {
			var k = this.cur;
			var l = this.x.length;
			while(k < l) {
				var n = this.x[k];
				k++;
				if(n.nodeType == Xml.Element && n._nodeName == name) {
					this.cur = k;
					return n;
				}
			}
			return null;
		}};
	}
	,elements: function() {
		if(this._children == null) throw "bad nodetype";
		return { cur : 0, x : this._children, hasNext : function() {
			var k = this.cur;
			var l = this.x.length;
			while(k < l) {
				if(this.x[k].nodeType == Xml.Element) break;
				k += 1;
			}
			this.cur = k;
			return k < l;
		}, next : function() {
			var k = this.cur;
			var l = this.x.length;
			while(k < l) {
				var n = this.x[k];
				k += 1;
				if(n.nodeType == Xml.Element) {
					this.cur = k;
					return n;
				}
			}
			return null;
		}};
	}
	,iterator: function() {
		if(this._children == null) throw "bad nodetype";
		return { cur : 0, x : this._children, hasNext : function() {
			return this.cur < this.x.length;
		}, next : function() {
			return this.x[this.cur++];
		}};
	}
	,exists: function(att) {
		if(this.nodeType != Xml.Element) throw "bad nodeType";
		return this._attributes.exists(att);
	}
	,set: function(att,value) {
		if(this.nodeType != Xml.Element) throw "bad nodeType";
		this._attributes.set(att,value);
	}
	,get: function(att) {
		if(this.nodeType != Xml.Element) throw "bad nodeType";
		return this._attributes.get(att);
	}
	,set_nodeValue: function(v) {
		if(this.nodeType == Xml.Element || this.nodeType == Xml.Document) throw "bad nodeType";
		return this._nodeValue = v;
	}
	,set_nodeName: function(n) {
		if(this.nodeType != Xml.Element) throw "bad nodeType";
		return this._nodeName = n;
	}
	,get_nodeName: function() {
		if(this.nodeType != Xml.Element) throw "bad nodeType";
		return this._nodeName;
	}
}
var com = {}
com.badgerhammer = {}
com.badgerhammer.string = {}
com.badgerhammer.string.BHStringTools = function() { }
com.badgerhammer.string.BHStringTools.__name__ = true;
com.badgerhammer.string.BHStringTools.startsWithNumber = function(string) {
	var firstCharacter = HxOverrides.substr(string,0,1);
	var _g = 0;
	while(_g < 9) {
		var i = _g++;
		if(firstCharacter == Std.string(i)) return true;
	}
	return false;
}
com.badgerhammer.tinytextworlds = {}
com.badgerhammer.tinytextworlds.GameLogic = function(fileName) {
	this._worldXml = new com.badgerhammer.tinytextworlds.WorldXml();
	this._worldXml.worldLoadCompleted.add($bind(this,this.onWorldLoadCompleted));
	this._worldXml.load(fileName);
};
com.badgerhammer.tinytextworlds.GameLogic.__name__ = true;
com.badgerhammer.tinytextworlds.GameLogic.prototype = {
	onAllActionsCompleted: function() {
		this._display.clear(true,false,false);
	}
	,onSayChoiceClicked: function(sayChoiceIndex) {
		this._actionResolver.doSayChoice(sayChoiceIndex);
	}
	,onFoundSayInActions: function(choice) {
		this._display.say(choice);
	}
	,onPlaceRefreshRequested: function() {
		this._currentPlayerLocation = this._worldXml.getPlayerLocation();
		this._currentPlace = new com.badgerhammer.tinytextworlds.TTWPlace(this._worldXml,this._currentPlayerLocation);
		this._display.displayPlace(this._currentPlace.getTextBlocks());
	}
	,onActionClicked: function(objectID,actionId) {
		this._actionResolver.resolve(objectID,actionId);
	}
	,onWorldLoadCompleted: function() {
		this._worldXml.worldLoadCompleted.removeAll();
		this._display = new com.badgerhammer.tinytextworlds.display.javascript.Display(this._worldXml.getActionPromptText(),this._worldXml.getCancelText());
		this._display.actionClicked.add($bind(this,this.onActionClicked));
		this._display.sayChoiceClicked.add($bind(this,this.onSayChoiceClicked));
		this._actionResolver = new com.badgerhammer.tinytextworlds.actions.ActionResolver(this._worldXml);
		this._actionResolver.foundSayInActions.add($bind(this,this.onFoundSayInActions));
		this._actionResolver.placeRefreshRequested.add($bind(this,this.onPlaceRefreshRequested));
		this._actionResolver.allActionsCompleted.add($bind(this,this.onAllActionsCompleted));
		this._actionResolver.resolve(this._worldXml.getStartUpObjectId(),this._worldXml.getStartUpActionId());
	}
}
com.badgerhammer.tinytextworlds.TTWObject = function(worldXmlRef,xml) {
	this._worldXmlRef = worldXmlRef;
	this._xml = xml;
	this._currentState = this._xml.get("state");
	this._currentStateXml = com.badgerhammer.xml.BHXmlTools.findNodeByAttributeValue(this._xml.elementsNamed("state"),"id",this._currentState);
};
com.badgerhammer.tinytextworlds.TTWObject.__name__ = true;
com.badgerhammer.tinytextworlds.TTWObject.prototype = {
	actionVisible: function(xml) {
		var visibleStates = xml.get("visibleStates").split(" ");
		if(visibleStates.length == 0) return false;
		var _g = 0;
		while(_g < visibleStates.length) {
			var visibleState = visibleStates[_g];
			++_g;
			if(com.badgerhammer.tinytextworlds.TTWXmlTools.doAttributeComparison(this._worldXmlRef,visibleState,"state")) return true;
		}
		return false;
	}
	,objectVisible: function() {
		if(this._currentStateXml.get("visible") == "false") return false;
		return true;
	}
	,getObjectActions: function() {
		var objectActions = new Array();
		var $it0 = this._xml.elementsNamed("action");
		while( $it0.hasNext() ) {
			var action = $it0.next();
			if(this.actionVisible(action)) {
				var hasChoice = false;
				if(action.elementsNamed("choice").hasNext()) hasChoice = true;
				objectActions.push(new com.badgerhammer.tinytextworlds.actions.ActionInfo(action.get("id"),action.get("name"),hasChoice));
			}
		}
		return objectActions;
	}
	,getTextBlocks: function() {
		var textBlocks = new Array();
		this._currentStateXml = com.badgerhammer.xml.BHXmlTools.findNodeByAttributeValue(this._xml.elementsNamed("state"),"id",this._currentState);
		var hasChanged = false;
		if(this._xml.get("hasChanged") == "true") {
			hasChanged = true;
			this._xml.set("hasChanged","false");
		}
		var preNameText = new com.badgerhammer.tinytextworlds.TextBlock("normal",this._currentStateXml.elementsNamed("preNameText").next().firstChild().toString(),null,hasChanged);
		var name = new com.badgerhammer.tinytextworlds.TextBlock("object",this._xml.get("name"),this._xml.get("id"),hasChanged,this.getObjectActions());
		var postNameText = new com.badgerhammer.tinytextworlds.TextBlock("normal",this._currentStateXml.elementsNamed("postNameText").next().firstChild().toString(),null,hasChanged);
		textBlocks.push(preNameText);
		textBlocks.push(name);
		textBlocks.push(postNameText);
		return textBlocks;
	}
}
com.badgerhammer.tinytextworlds.TTWPlace = function(worldXml,currentPlayerLocation) {
	this._worldXmlRef = worldXml;
	this._xml = worldXml.getPlace(currentPlayerLocation);
	this.makeObjects(worldXml);
	this.addGlobalObjects(worldXml);
};
com.badgerhammer.tinytextworlds.TTWPlace.__name__ = true;
com.badgerhammer.tinytextworlds.TTWPlace.prototype = {
	getTextBlocks: function() {
		var textBlocks = new Array();
		var _g = 0, _g1 = this._objects;
		while(_g < _g1.length) {
			var object = _g1[_g];
			++_g;
			if(!object.objectVisible()) continue;
			var objectTextBlocks = object.getTextBlocks();
			textBlocks.push(objectTextBlocks[0]);
			textBlocks.push(objectTextBlocks[1]);
			textBlocks.push(objectTextBlocks[2]);
		}
		return textBlocks;
	}
	,addGlobalObjects: function(worldXml) {
		if(!worldXml.isUsingGlobalObjects()) return;
		var objectsXmlIterator = worldXml.getPlace("global").elementsNamed("object");
		while(objectsXmlIterator.hasNext()) this._objects.push(new com.badgerhammer.tinytextworlds.TTWObject(this._worldXmlRef,worldXml.getObject(objectsXmlIterator.next().get("id"))));
	}
	,makeObjects: function(worldXml) {
		this._objects = new Array();
		var objectsXmlIterator = this._xml.elementsNamed("object");
		while(objectsXmlIterator.hasNext()) this._objects.push(new com.badgerhammer.tinytextworlds.TTWObject(this._worldXmlRef,worldXml.getObject(objectsXmlIterator.next().get("id"))));
	}
}
com.badgerhammer.tinytextworlds.TTWXmlTools = function() { }
com.badgerhammer.tinytextworlds.TTWXmlTools.__name__ = true;
com.badgerhammer.tinytextworlds.TTWXmlTools.doAttributeComparison = function(worldXmlRef,allowedValuesString,targetAttribute) {
	if(allowedValuesString == "") return false;
	if(allowedValuesString == "*") return true;
	var allowedValues = allowedValuesString.split("&&");
	var foundVisibleState = false;
	var _g1 = 0, _g = allowedValues.length;
	while(_g1 < _g) {
		var i = _g1++;
		var allowedValueInfo = allowedValues[i].split(":");
		if(allowedValueInfo.length < 2) haxe.Log.trace("Script Error! AllowedValues must be * or \"\" or object:value. Looking for \"" + allowedValuesString + "\"",{ fileName : "TTWXmlTools.hx", lineNumber : 28, className : "com.badgerhammer.tinytextworlds.TTWXmlTools", methodName : "doAttributeComparison"});
		var currentValue = worldXmlRef.getObject(allowedValueInfo[0]).get(targetAttribute);
		var allowedValue = allowedValueInfo[1];
		StringTools.htmlUnescape(allowedValue);
		var firstCharacter = HxOverrides.substr(allowedValue,0,1);
		if(firstCharacter == "{" || firstCharacter == "}") {
			if(!com.badgerhammer.tinytextworlds.TTWXmlTools.greterThanOrLessThanComparison(currentValue,firstCharacter,HxOverrides.substr(allowedValue,1,null))) return false;
		} else if(allowedValue != currentValue) return false;
	}
	return true;
}
com.badgerhammer.tinytextworlds.TTWXmlTools.acceptableStartValuesComparison = function(currentValue,allowedValue) {
	if(allowedValue == "*") return true;
	var firstCharacter = HxOverrides.substr(allowedValue,0,1);
	if(firstCharacter == "{" || firstCharacter == "}") return com.badgerhammer.tinytextworlds.TTWXmlTools.greterThanOrLessThanComparison(currentValue,firstCharacter,HxOverrides.substr(allowedValue,1,null)); else if(allowedValue == currentValue) return true;
	return false;
}
com.badgerhammer.tinytextworlds.TTWXmlTools.greterThanOrLessThanComparison = function(currentValue,firstCharacter,allowedValue) {
	if(com.badgerhammer.string.BHStringTools.startsWithNumber(allowedValue) && com.badgerhammer.string.BHStringTools.startsWithNumber(currentValue)) {
		if(firstCharacter == "}") {
			if(Std.parseFloat(currentValue) > Std.parseFloat(allowedValue)) return true;
		} else if(Std.parseFloat(currentValue) < Std.parseFloat(allowedValue)) return true;
	}
	return false;
}
com.badgerhammer.tinytextworlds.TextBlock = function(type,text,id,hasChanged,actions) {
	if(hasChanged == null) hasChanged = false;
	if(id == null) id = "";
	this.type = type;
	this.text = text;
	this.id = id;
	this.actions = actions;
	this.hasChanged = hasChanged;
};
com.badgerhammer.tinytextworlds.TextBlock.__name__ = true;
com.badgerhammer.tinytextworlds.WorldXml = function() {
	this.worldLoadCompleted = new msignal.Signal0();
};
com.badgerhammer.tinytextworlds.WorldXml.__name__ = true;
com.badgerhammer.tinytextworlds.WorldXml.prototype = {
	isUsingGlobalObjects: function() {
		return this._xml.elements().next().elementsNamed("useGlobalObjects").next().get("flag") == "true";
	}
	,getStartUpActionId: function() {
		return this._xml.elements().next().elementsNamed("startUpAction").next().get("actionId");
	}
	,getStartUpObjectId: function() {
		return this._xml.elements().next().elementsNamed("startUpAction").next().get("objectId");
	}
	,getActionPromptText: function() {
		return this._xml.elements().next().elementsNamed("actionPrompt").next().get("defaultText");
	}
	,getCancelText: function() {
		return this._xml.elements().next().elementsNamed("cancel").next().get("defaultText");
	}
	,getObjectAction: function(objectId,actionId) {
		var objectsXmlIterator = this._xml.elements().next().elementsNamed("objects").next().elementsNamed("object");
		var actionsXmlIterator = com.badgerhammer.xml.BHXmlTools.findNodeByAttributeValue(objectsXmlIterator,"id",objectId).elementsNamed("action");
		return com.badgerhammer.xml.BHXmlTools.findNodeByAttributeValue(actionsXmlIterator,"id",actionId);
	}
	,getObject: function(objectId) {
		var objectsXmlIterator = this._xml.elements().next().elementsNamed("objects").next().elementsNamed("object");
		var xml = com.badgerhammer.xml.BHXmlTools.findNodeByAttributeValue(objectsXmlIterator,"id",objectId);
		return xml;
	}
	,getPlace: function(placeId) {
		var placesXmlIterator = this._xml.elements().next().elementsNamed("places").next().elementsNamed("place");
		return com.badgerhammer.xml.BHXmlTools.findNodeByAttributeValue(placesXmlIterator,"id",placeId);
	}
	,getPlayer: function() {
		return this._xml.elements().next().elementsNamed("player").next();
	}
	,getPlayerLocation: function() {
		return this._xml.elements().next().elementsNamed("player").next().get("location");
	}
	,loadComplete: function(xml) {
		this._xml = xml;
		this._xmlLoader = null;
		this.worldLoadCompleted.dispatch();
	}
	,load: function(url) {
		this._xmlLoader = new com.badgerhammer.xml.general.XmlLoader();
		this._xmlLoader.loadXML(url,$bind(this,this.loadComplete));
	}
	,report: function() {
		return this._xml;
	}
}
com.badgerhammer.tinytextworlds.actions = {}
com.badgerhammer.tinytextworlds.actions.ActionInfo = function(actionId,actionName,hasChoice,objectId) {
	if(objectId == null) objectId = "not set";
	if(hasChoice == null) hasChoice = false;
	this.actionId = actionId;
	this.actionName = actionName;
	this.hasChoice = hasChoice;
	this.objectId = objectId;
};
com.badgerhammer.tinytextworlds.actions.ActionInfo.__name__ = true;
com.badgerhammer.tinytextworlds.actions.ActionResolver = function(worldXml) {
	this._worldXml = worldXml;
	this.foundSayInActions = new msignal.Signal1();
	this.allActionsCompleted = new msignal.Signal0();
	this.placeRefreshRequested = new msignal.Signal0();
};
com.badgerhammer.tinytextworlds.actions.ActionResolver.__name__ = true;
com.badgerhammer.tinytextworlds.actions.ActionResolver.prototype = {
	doSayChoice: function(choiceIndex) {
		var currentSayChoice = this._currentSayChoice;
		this._currentSayChoice = null;
		this._currentObject = this._worldXml.getObject(currentSayChoice.targetObjects[choiceIndex]);
		this.processActionSteps(currentSayChoice.actions[choiceIndex]);
	}
	,doSet: function(setXml) {
		var targetObject;
		if(setXml.get("targetObjectId") == "__player__") targetObject = this._worldXml.getPlayer(); else {
			targetObject = this._worldXml.getObject(setXml.get("targetObjectId"));
			targetObject.set("hasChanged","true");
		}
		var acceptableStartValues = setXml.get("acceptableStartValues").split(" ");
		var acceptableValueFound = false;
		var currentAttributeValue = targetObject.get(setXml.get("targetAttribute"));
		var _g = 0;
		while(_g < acceptableStartValues.length) {
			var acceptableValue = acceptableStartValues[_g];
			++_g;
			acceptableValueFound = com.badgerhammer.tinytextworlds.TTWXmlTools.acceptableStartValuesComparison(currentAttributeValue,acceptableValue);
			if(acceptableValueFound) break;
		}
		if(acceptableValueFound) {
			var firstCharacter = HxOverrides.substr(setXml.get("newValue"),0,1);
			if(firstCharacter == "+" || firstCharacter == "-") com.badgerhammer.xml.BHXmlTools.doArithmeticOnAttribute(targetObject,setXml.get("targetAttribute"),firstCharacter,HxOverrides.substr(setXml.get("newValue"),1,null)); else targetObject.set(setXml.get("targetAttribute"),setXml.get("newValue"));
		}
	}
	,processActionSteps: function(action) {
		var actionSteps = action.elements();
		var changedWorld = false;
		while( actionSteps.hasNext() ) {
			var xml = actionSteps.next();
			if(xml.get_nodeName() == "set") {
				this.doSet(xml);
				changedWorld = true;
			} else if(xml.get_nodeName() == "say") {
				this._currentSayChoice = new com.badgerhammer.tinytextworlds.actions.choice.SayChoice(xml.elements(),this._worldXml,this._currentObject);
				this.foundSayInActions.dispatch(this._currentSayChoice);
				if(this._currentSayChoice.choices.length == 0) this._currentSayChoice = null;
			}
		}
		if(this._currentSayChoice == null) {
			this.allActionsCompleted.dispatch();
			this.placeRefreshRequested.dispatch();
			this.clearChecks();
			return;
		}
		if(changedWorld && this._currentSayChoice == null) this.placeRefreshRequested.dispatch();
	}
	,findAction: function(objectID,actionId) {
		this._currentObject = this._worldXml.getObject(objectID);
		this._currentAction = this._worldXml.getObjectAction(objectID,actionId);
	}
	,resolve: function(objectID,actionId) {
		this.findAction(objectID,actionId);
		this.processActionSteps(this._currentAction);
	}
	,clearChecks: function() {
		this._haveMadeChoice = false;
	}
}
com.badgerhammer.tinytextworlds.actions.choice = {}
com.badgerhammer.tinytextworlds.actions.choice.Choice = function(choiceSteps,worldXml,currentObject) {
	this.choices = new Array();
	this.actions = new Array();
	this.targetObjects = new Array();
	this.populatedChoiceFound = false;
	while( choiceSteps.hasNext() ) {
		var xml = choiceSteps.next();
		if(xml.get_nodeName() == "text") this.setText(com.badgerhammer.tinytextworlds.string.TTWStringTools.regExpString(xml.firstChild().toString(),"__objectName__",currentObject.get("name"))); else if(xml.get_nodeName() == "option") {
			var nodeType = xml.get("type");
			if(nodeType == "object") {
				var targetObjectId = xml.get("targetObjectId");
				var targetActionId = xml.get("actionId");
				var visibleStates = xml.get("visibleStates").split(" ");
				var choosableStateFound = false;
				var _g = 0;
				while(_g < visibleStates.length) {
					var state = visibleStates[_g];
					++_g;
					if(com.badgerhammer.tinytextworlds.TTWXmlTools.doAttributeComparison(worldXml,state,"state")) {
						choosableStateFound = true;
						break;
					}
				}
				if(!choosableStateFound) continue;
				if(HxOverrides.substr(xml.get("text"),0,13) == "nameOfObject:") {
					var choiceObjectId = xml.get("text").split(":")[1];
					var choiceObject = worldXml.getObject(choiceObjectId);
					this.addChoice(choiceObject.get("name"));
				} else this.addChoice(xml.get("text"));
				this.addAction(worldXml.getObjectAction(targetObjectId,targetActionId));
				this.addTargetObject(targetObjectId);
				if(choosableStateFound) this.populatedChoiceFound = true;
			}
		}
	}
};
com.badgerhammer.tinytextworlds.actions.choice.Choice.__name__ = true;
com.badgerhammer.tinytextworlds.actions.choice.Choice.prototype = {
	addTargetObject: function(newTargetObject) {
		this.targetObjects.push(newTargetObject);
	}
	,addAction: function(newAction) {
		this.actions.push(newAction);
	}
	,addChoice: function(newChoice) {
		this.choices.push(newChoice);
	}
	,setText: function(newtext) {
		this.text = newtext;
	}
}
com.badgerhammer.tinytextworlds.actions.choice.SayChoice = function(choiceSteps,worldXml,currentObject) {
	com.badgerhammer.tinytextworlds.actions.choice.Choice.call(this,choiceSteps,worldXml,currentObject);
};
com.badgerhammer.tinytextworlds.actions.choice.SayChoice.__name__ = true;
com.badgerhammer.tinytextworlds.actions.choice.SayChoice.__super__ = com.badgerhammer.tinytextworlds.actions.choice.Choice;
com.badgerhammer.tinytextworlds.actions.choice.SayChoice.prototype = $extend(com.badgerhammer.tinytextworlds.actions.choice.Choice.prototype,{
});
com.badgerhammer.tinytextworlds.display = {}
com.badgerhammer.tinytextworlds.display.IDisplay = function() { }
com.badgerhammer.tinytextworlds.display.IDisplay.__name__ = true;
com.badgerhammer.tinytextworlds.display.javascript = {}
com.badgerhammer.tinytextworlds.display.javascript.ActionDisplay = function(actionPromptText,cancelText,clearCallback) {
	this._document = js.Browser.document;
	this._clearCallback = clearCallback;
	this.actionClicked = new msignal.Signal2();
	this._actionPromptText = actionPromptText;
	this._cancelText = cancelText;
	if(this._actionPromptText != "") this._useActionPrompt = true; else this._useActionPrompt = false;
	this._ttwActionsDiv = this._document.getElementById("TTWActions");
	this._interactiveDomElements = new Array();
};
com.badgerhammer.tinytextworlds.display.javascript.ActionDisplay.__name__ = true;
com.badgerhammer.tinytextworlds.display.javascript.ActionDisplay.prototype = {
	removeListeners: function() {
		while(this._interactiveDomElements.length > 0) this._interactiveDomElements.shift().onclick = null;
	}
	,clear: function() {
		this.removeListeners();
		this._ttwActionsDiv.innerHTML = "";
	}
	,action_onClick: function(e) {
		var _g = 0, _g1 = this._interactiveDomElements;
		while(_g < _g1.length) {
			var element = _g1[_g];
			++_g;
			if(element.id != "cancel-action") element.className = "action interactive";
		}
		var clickedElement = e.target;
		clickedElement.className = "action interactive action-clicked";
		var actionId = clickedElement.id.split("-")[1];
		this.actionClicked.dispatch(this._objectId,actionId);
	}
	,cancel_onClick: function(e) {
		this._clearCallback(true,false,false);
	}
	,displayActions: function(actions,objectId,objectName) {
		this._objectId = objectId;
		this.clear();
		this._ttwActionsDiv.appendChild(this._document.createElement("hr"));
		var element;
		if(this._useActionPrompt) {
			element = this._document.createElement("span");
			element.id = "actions-prompt";
			element.innerHTML = this._actionPromptText + " " + objectName + "<br/>";
			this._ttwActionsDiv.appendChild(element);
		}
		var _g1 = 0, _g = actions.length;
		while(_g1 < _g) {
			var index = _g1++;
			if(index != 0) {
				element = this._document.createElement("br");
				this._ttwActionsDiv.appendChild(element);
			}
			element = this._document.createElement("a");
			element.id = "action-" + actions[index].actionId;
			element.className = "action interactive";
			element.innerHTML = actions[index].actionName;
			if(actions[index].hasChoice) {
				element.setAttribute("href","#TTWChoice");
				element.innerHTML += "...";
			} else element.setAttribute("href","#TTWSay");
			this._ttwActionsDiv.appendChild(element);
			element.onclick = $bind(this,this.action_onClick);
			this._interactiveDomElements.push(element);
		}
		element = this._document.createElement("br");
		this._ttwActionsDiv.appendChild(element);
		element = this._document.createElement("a");
		element.setAttribute("href","#TTWSay");
		element.innerHTML = this._cancelText;
		element.className = "interactive cancel";
		element.id = "cancel-action";
		this._ttwActionsDiv.appendChild(element);
		element.onclick = $bind(this,this.cancel_onClick);
		this._interactiveDomElements.push(element);
	}
}
com.badgerhammer.tinytextworlds.display.javascript.Display = function(actionPromptText,cancelText) {
	this.actionClicked = new msignal.Signal2();
	this.sayChoiceClicked = new msignal.Signal1();
	this._actionDisplay = new com.badgerhammer.tinytextworlds.display.javascript.ActionDisplay(actionPromptText,cancelText,$bind(this,this.clear));
	this._actionDisplay.actionClicked.add($bind(this,this.onActionClicked));
	this._placeDisplay = new com.badgerhammer.tinytextworlds.display.javascript.PlaceDisplay($bind(this,this.displayActions));
	this._sayDisplay = new com.badgerhammer.tinytextworlds.display.javascript.SayDisplay();
	this._sayDisplay.sayChoiceClicked.add($bind(this,this.onSayChoiceClicked));
};
com.badgerhammer.tinytextworlds.display.javascript.Display.__name__ = true;
com.badgerhammer.tinytextworlds.display.javascript.Display.__interfaces__ = [com.badgerhammer.tinytextworlds.display.IDisplay];
com.badgerhammer.tinytextworlds.display.javascript.Display.prototype = {
	clear: function(clearActionDisplay,clearPlaceDisplay,clearSayDisplay) {
		if(clearActionDisplay) this._actionDisplay.clear();
		if(clearPlaceDisplay) this._placeDisplay.clear();
		if(clearSayDisplay) this._sayDisplay.clear();
	}
	,displayActions: function(actions,objectId,objectName) {
		this._sayDisplay.clear();
		this._actionDisplay.displayActions(actions,objectId,objectName);
	}
	,displayPlace: function(placeTextBlocks) {
		this._placeDisplay.displayPlace(placeTextBlocks);
	}
	,say: function(choice) {
		this._sayDisplay.display(choice);
		if(this._sayDisplay.hasActions) {
			this._placeDisplay.clear();
			this._actionDisplay.clear();
		}
	}
	,onSayChoiceClicked: function(sayChoiceId) {
		this.sayChoiceClicked.dispatch(sayChoiceId);
	}
	,onActionClicked: function(objectId,actionId) {
		this.actionClicked.dispatch(objectId,actionId);
	}
}
com.badgerhammer.tinytextworlds.display.javascript.PlaceDisplay = function(displayActionFunction) {
	this._document = js.Browser.document;
	this._displayActionFunction = displayActionFunction;
	this._interactiveDomElements = new Array();
	this._ttwPlaceDiv = this._document.getElementById("TTWPlace");
};
com.badgerhammer.tinytextworlds.display.javascript.PlaceDisplay.__name__ = true;
com.badgerhammer.tinytextworlds.display.javascript.PlaceDisplay.prototype = {
	removeListeners: function() {
		while(this._interactiveDomElements.length > 0) this._interactiveDomElements.shift().onclick = null;
	}
	,clear: function() {
		this.removeListeners();
		this._ttwPlaceDiv.innerHTML = "";
	}
	,interactiveDomElement_onClick: function(e) {
		this.resetClickedButtons();
		var clickedElement = e.target;
		clickedElement.className += " object-clicked";
		var actionsArray = new Array();
		var textBlockId = "";
		var textBlockName = "";
		var _g = 0, _g1 = this._placeTextBlocks;
		while(_g < _g1.length) {
			var textBlock = _g1[_g];
			++_g;
			if(textBlock.id == clickedElement.id) {
				actionsArray = textBlock.actions;
				textBlockId = textBlock.id;
				textBlockName = textBlock.text;
				break;
			}
		}
		this._displayActionFunction(actionsArray,textBlockId,textBlockName);
	}
	,resetClickedButtons: function() {
		var _g = 0, _g1 = this._interactiveDomElements;
		while(_g < _g1.length) {
			var element = _g1[_g];
			++_g;
			var classes = element.className.split(" ");
			element.className = "";
			var _g2 = 0;
			while(_g2 < classes.length) {
				var className = classes[_g2];
				++_g2;
				if(className == "object-clicked") continue;
				element.className += className + " ";
			}
		}
	}
	,displayPlace: function(placeTextBlocks) {
		this.clear();
		this._placeTextBlocks = placeTextBlocks;
		var firstSentence = true;
		var createdTextBlocks = 0;
		var _g = 0;
		while(_g < placeTextBlocks.length) {
			var textBlock = placeTextBlocks[_g];
			++_g;
			var element;
			if(textBlock.id != "") {
				if(textBlock.actions.length > 0) {
					element = this._document.createElement("a");
					element.onclick = $bind(this,this.interactiveDomElement_onClick);
					this._interactiveDomElements.push(element);
					element.className = textBlock.type;
					element.className += "-has-actions interactive";
					element.setAttribute("href","#TTWActions");
				} else {
					element = this._document.createElement("span");
					element.className = textBlock.type;
					element.className += "-no-actions interactive";
				}
				element.id = textBlock.id;
				element.innerHTML = textBlock.text;
			} else {
				element = this._document.createElement("span");
				element.className = textBlock.type;
				if(firstSentence) {
					element.innerHTML = "<span class=\"first-letter\">" + textBlock.text.charAt(0) + "</span>";
					element.innerHTML += HxOverrides.substr(textBlock.text,1,null);
					firstSentence = false;
				} else element.innerHTML = textBlock.text;
			}
			if(textBlock.hasChanged) element.className += " has-changed";
			createdTextBlocks++;
			if(createdTextBlocks == 3) {
				createdTextBlocks = 0;
				element.innerHTML += " ";
			}
			this._ttwPlaceDiv.appendChild(element);
		}
	}
}
com.badgerhammer.tinytextworlds.display.javascript.SayDisplay = function() {
	this._document = js.Browser.document;
	this.sayChoiceClicked = new msignal.Signal1();
	this._ttwSayDiv = this._document.getElementById("TTWSay");
	this._interactiveDomElements = new Array();
};
com.badgerhammer.tinytextworlds.display.javascript.SayDisplay.__name__ = true;
com.badgerhammer.tinytextworlds.display.javascript.SayDisplay.prototype = {
	removeListeners: function() {
		while(this._interactiveDomElements.length > 0) this._interactiveDomElements.shift().onclick = null;
	}
	,clear: function() {
		this.removeListeners();
		this._ttwSayDiv.innerHTML = "";
	}
	,choice_onClick: function(e) {
		var clickedElement = e.target;
		var choiceIndex = Std.parseInt(clickedElement.id.split("-")[1]);
		this.clear();
		this.sayChoiceClicked.dispatch(choiceIndex);
	}
	,display: function(choice) {
		this.clear();
		var element;
		element = this._document.createElement("span");
		element.id = "choice-query";
		element.innerHTML = com.badgerhammer.tinytextworlds.string.TTWStringTools.stripCDataEnclosure(choice.text) + "<br/>";
		this._ttwSayDiv.appendChild(element);
		var choiceCount = choice.choices.length;
		this.hasActions = false;
		if(choiceCount > 0) this.hasActions = true;
		var _g = 0;
		while(_g < choiceCount) {
			var index = _g++;
			if(index != 0) {
				element = this._document.createElement("br");
				this._ttwSayDiv.appendChild(element);
			}
			element = this._document.createElement("a");
			element.setAttribute("href","#TTWSay");
			element.id = "choice-" + index;
			element.className = "action interactive";
			element.innerHTML = choice.choices[index];
			this._ttwSayDiv.appendChild(element);
			element.onclick = $bind(this,this.choice_onClick);
			this._interactiveDomElements.push(element);
		}
	}
}
com.badgerhammer.tinytextworlds.string = {}
com.badgerhammer.tinytextworlds.string.TTWStringTools = function() {
};
com.badgerhammer.tinytextworlds.string.TTWStringTools.__name__ = true;
com.badgerhammer.tinytextworlds.string.TTWStringTools.regExpString = function(targetString,replaceThis,replaceWith) {
	var objectRegExp;
	switch(replaceThis) {
	case "__objectName__":
		objectRegExp = new EReg("__objectName__","");
		break;
	case "__cancel__":
		objectRegExp = new EReg("__cancel__","");
		break;
	default:
		objectRegExp = new EReg("__objectName__","");
	}
	return objectRegExp.replace(targetString,replaceWith);
}
com.badgerhammer.tinytextworlds.string.TTWStringTools.stripCDataEnclosure = function(targetString) {
	var objectRegExp;
	objectRegExp = new EReg("<!\\[CDATA\\[","");
	targetString = objectRegExp.replace(targetString,"");
	objectRegExp = new EReg("\\]\\]>","");
	targetString = objectRegExp.replace(targetString,"");
	return targetString;
}
com.badgerhammer.xml = {}
com.badgerhammer.xml.BHXmlTools = function() { }
com.badgerhammer.xml.BHXmlTools.__name__ = true;
com.badgerhammer.xml.BHXmlTools.findNodeByAttributeValue = function(iterator,attributeName,attributeValue) {
	var xml = null;
	var foundMatch = false;
	while(iterator.hasNext()) {
		xml = iterator.next();
		if(xml.get(attributeName) == attributeValue) {
			foundMatch = true;
			break;
		}
	}
	if(!foundMatch) haxe.Log.trace("Script Error! No match found:" + " " + attributeName + " = " + attributeValue,{ fileName : "BHXmlTools.hx", lineNumber : 41, className : "com.badgerhammer.xml.BHXmlTools", methodName : "findNodeByAttributeValue"});
	return xml;
}
com.badgerhammer.xml.BHXmlTools.doArithmeticOnAttribute = function(targetNode,targetAttribute,arithmeticMode,number) {
	var change = Std.parseFloat(number);
	if(arithmeticMode == "-") change *= -1;
	var currentValue = targetNode.get(targetAttribute);
	if(com.badgerhammer.string.BHStringTools.startsWithNumber(currentValue)) targetNode.set(targetAttribute,Std.string(Std.parseFloat(currentValue) + change)); else haxe.Log.trace("No Number found",{ fileName : "BHXmlTools.hx", lineNumber : 62, className : "com.badgerhammer.xml.BHXmlTools", methodName : "doArithmeticOnAttribute"});
}
com.badgerhammer.xml.ILoadsXML = function() { }
com.badgerhammer.xml.ILoadsXML.__name__ = true;
com.badgerhammer.xml.general = {}
com.badgerhammer.xml.general.XmlLoader = function() {
};
com.badgerhammer.xml.general.XmlLoader.__name__ = true;
com.badgerhammer.xml.general.XmlLoader.__interfaces__ = [com.badgerhammer.xml.ILoadsXML];
com.badgerhammer.xml.general.XmlLoader.prototype = {
	loadXML: function(url,completeCallback) {
		try {
			var xmlString = haxe.Http.requestUrl(url);
			completeCallback(Xml.parse(xmlString));
		} catch( msg ) {
			if( js.Boot.__instanceof(msg,String) ) {
			} else throw(msg);
		}
	}
}
var haxe = {}
haxe.Http = function(url) {
	this.url = url;
	this.headers = new haxe.ds.StringMap();
	this.params = new haxe.ds.StringMap();
	this.async = true;
};
haxe.Http.__name__ = true;
haxe.Http.requestUrl = function(url) {
	var h = new haxe.Http(url);
	h.async = false;
	var r = null;
	h.onData = function(d) {
		r = d;
	};
	h.onError = function(e) {
		throw e;
	};
	h.request(false);
	return r;
}
haxe.Http.prototype = {
	onStatus: function(status) {
	}
	,onError: function(msg) {
	}
	,onData: function(data) {
	}
	,request: function(post) {
		var me = this;
		me.responseData = null;
		var r = js.Browser.createXMLHttpRequest();
		var onreadystatechange = function(_) {
			if(r.readyState != 4) return;
			var s = (function($this) {
				var $r;
				try {
					$r = r.status;
				} catch( e ) {
					$r = null;
				}
				return $r;
			}(this));
			if(s == undefined) s = null;
			if(s != null) me.onStatus(s);
			if(s != null && s >= 200 && s < 400) me.onData(me.responseData = r.responseText); else if(s == null) me.onError("Failed to connect or resolve host"); else switch(s) {
			case 12029:
				me.onError("Failed to connect to host");
				break;
			case 12007:
				me.onError("Unknown host");
				break;
			default:
				me.responseData = r.responseText;
				me.onError("Http Error #" + r.status);
			}
		};
		if(this.async) r.onreadystatechange = onreadystatechange;
		var uri = this.postData;
		if(uri != null) post = true; else {
			var $it0 = this.params.keys();
			while( $it0.hasNext() ) {
				var p = $it0.next();
				if(uri == null) uri = ""; else uri += "&";
				uri += StringTools.urlEncode(p) + "=" + StringTools.urlEncode(this.params.get(p));
			}
		}
		try {
			if(post) r.open("POST",this.url,this.async); else if(uri != null) {
				var question = this.url.split("?").length <= 1;
				r.open("GET",this.url + (question?"?":"&") + uri,this.async);
				uri = null;
			} else r.open("GET",this.url,this.async);
		} catch( e ) {
			this.onError(e.toString());
			return;
		}
		if(this.headers.get("Content-Type") == null && post && this.postData == null) r.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
		var $it1 = this.headers.keys();
		while( $it1.hasNext() ) {
			var h = $it1.next();
			r.setRequestHeader(h,this.headers.get(h));
		}
		r.send(uri);
		if(!this.async) onreadystatechange(null);
	}
}
haxe.Log = function() { }
haxe.Log.__name__ = true;
haxe.Log.trace = function(v,infos) {
	js.Boot.__trace(v,infos);
}
haxe.ds = {}
haxe.ds.StringMap = function() {
	this.h = { };
};
haxe.ds.StringMap.__name__ = true;
haxe.ds.StringMap.__interfaces__ = [IMap];
haxe.ds.StringMap.prototype = {
	keys: function() {
		var a = [];
		for( var key in this.h ) {
		if(this.h.hasOwnProperty(key)) a.push(key.substr(1));
		}
		return HxOverrides.iter(a);
	}
	,exists: function(key) {
		return this.h.hasOwnProperty("$" + key);
	}
	,get: function(key) {
		return this.h["$" + key];
	}
	,set: function(key,value) {
		this.h["$" + key] = value;
	}
}
haxe.xml = {}
haxe.xml.Parser = function() { }
haxe.xml.Parser.__name__ = true;
haxe.xml.Parser.parse = function(str) {
	var doc = Xml.createDocument();
	haxe.xml.Parser.doParse(str,0,doc);
	return doc;
}
haxe.xml.Parser.doParse = function(str,p,parent) {
	if(p == null) p = 0;
	var xml = null;
	var state = 1;
	var next = 1;
	var aname = null;
	var start = 0;
	var nsubs = 0;
	var nbrackets = 0;
	var c = str.charCodeAt(p);
	var buf = new StringBuf();
	while(!(c != c)) {
		switch(state) {
		case 0:
			switch(c) {
			case 10:case 13:case 9:case 32:
				break;
			default:
				state = next;
				continue;
			}
			break;
		case 1:
			switch(c) {
			case 60:
				state = 0;
				next = 2;
				break;
			default:
				start = p;
				state = 13;
				continue;
			}
			break;
		case 13:
			if(c == 60) {
				var child = Xml.createPCData(buf.b + HxOverrides.substr(str,start,p - start));
				buf = new StringBuf();
				parent.addChild(child);
				nsubs++;
				state = 0;
				next = 2;
			} else if(c == 38) {
				buf.addSub(str,start,p - start);
				state = 18;
				next = 13;
				start = p + 1;
			}
			break;
		case 17:
			if(c == 93 && str.charCodeAt(p + 1) == 93 && str.charCodeAt(p + 2) == 62) {
				var child = Xml.createCData(HxOverrides.substr(str,start,p - start));
				parent.addChild(child);
				nsubs++;
				p += 2;
				state = 1;
			}
			break;
		case 2:
			switch(c) {
			case 33:
				if(str.charCodeAt(p + 1) == 91) {
					p += 2;
					if(HxOverrides.substr(str,p,6).toUpperCase() != "CDATA[") throw "Expected <![CDATA[";
					p += 5;
					state = 17;
					start = p + 1;
				} else if(str.charCodeAt(p + 1) == 68 || str.charCodeAt(p + 1) == 100) {
					if(HxOverrides.substr(str,p + 2,6).toUpperCase() != "OCTYPE") throw "Expected <!DOCTYPE";
					p += 8;
					state = 16;
					start = p + 1;
				} else if(str.charCodeAt(p + 1) != 45 || str.charCodeAt(p + 2) != 45) throw "Expected <!--"; else {
					p += 2;
					state = 15;
					start = p + 1;
				}
				break;
			case 63:
				state = 14;
				start = p;
				break;
			case 47:
				if(parent == null) throw "Expected node name";
				start = p + 1;
				state = 0;
				next = 10;
				break;
			default:
				state = 3;
				start = p;
				continue;
			}
			break;
		case 3:
			if(!(c >= 97 && c <= 122 || c >= 65 && c <= 90 || c >= 48 && c <= 57 || c == 58 || c == 46 || c == 95 || c == 45)) {
				if(p == start) throw "Expected node name";
				xml = Xml.createElement(HxOverrides.substr(str,start,p - start));
				parent.addChild(xml);
				state = 0;
				next = 4;
				continue;
			}
			break;
		case 4:
			switch(c) {
			case 47:
				state = 11;
				nsubs++;
				break;
			case 62:
				state = 9;
				nsubs++;
				break;
			default:
				state = 5;
				start = p;
				continue;
			}
			break;
		case 5:
			if(!(c >= 97 && c <= 122 || c >= 65 && c <= 90 || c >= 48 && c <= 57 || c == 58 || c == 46 || c == 95 || c == 45)) {
				var tmp;
				if(start == p) throw "Expected attribute name";
				tmp = HxOverrides.substr(str,start,p - start);
				aname = tmp;
				if(xml.exists(aname)) throw "Duplicate attribute";
				state = 0;
				next = 6;
				continue;
			}
			break;
		case 6:
			switch(c) {
			case 61:
				state = 0;
				next = 7;
				break;
			default:
				throw "Expected =";
			}
			break;
		case 7:
			switch(c) {
			case 34:case 39:
				state = 8;
				start = p;
				break;
			default:
				throw "Expected \"";
			}
			break;
		case 8:
			if(c == str.charCodeAt(start)) {
				var val = HxOverrides.substr(str,start + 1,p - start - 1);
				xml.set(aname,val);
				state = 0;
				next = 4;
			}
			break;
		case 9:
			p = haxe.xml.Parser.doParse(str,p,xml);
			start = p;
			state = 1;
			break;
		case 11:
			switch(c) {
			case 62:
				state = 1;
				break;
			default:
				throw "Expected >";
			}
			break;
		case 12:
			switch(c) {
			case 62:
				if(nsubs == 0) parent.addChild(Xml.createPCData(""));
				return p;
			default:
				throw "Expected >";
			}
			break;
		case 10:
			if(!(c >= 97 && c <= 122 || c >= 65 && c <= 90 || c >= 48 && c <= 57 || c == 58 || c == 46 || c == 95 || c == 45)) {
				if(start == p) throw "Expected node name";
				var v = HxOverrides.substr(str,start,p - start);
				if(v != parent.get_nodeName()) throw "Expected </" + parent.get_nodeName() + ">";
				state = 0;
				next = 12;
				continue;
			}
			break;
		case 15:
			if(c == 45 && str.charCodeAt(p + 1) == 45 && str.charCodeAt(p + 2) == 62) {
				parent.addChild(Xml.createComment(HxOverrides.substr(str,start,p - start)));
				p += 2;
				state = 1;
			}
			break;
		case 16:
			if(c == 91) nbrackets++; else if(c == 93) nbrackets--; else if(c == 62 && nbrackets == 0) {
				parent.addChild(Xml.createDocType(HxOverrides.substr(str,start,p - start)));
				state = 1;
			}
			break;
		case 14:
			if(c == 63 && str.charCodeAt(p + 1) == 62) {
				p++;
				var str1 = HxOverrides.substr(str,start + 1,p - start - 2);
				parent.addChild(Xml.createProcessingInstruction(str1));
				state = 1;
			}
			break;
		case 18:
			if(c == 59) {
				var s = HxOverrides.substr(str,start,p - start);
				if(s.charCodeAt(0) == 35) {
					var i = s.charCodeAt(1) == 120?Std.parseInt("0" + HxOverrides.substr(s,1,s.length - 1)):Std.parseInt(HxOverrides.substr(s,1,s.length - 1));
					buf.b += Std.string(String.fromCharCode(i));
				} else if(!haxe.xml.Parser.escapes.exists(s)) buf.b += Std.string("&" + s + ";"); else buf.b += Std.string(haxe.xml.Parser.escapes.get(s));
				start = p + 1;
				state = next;
			}
			break;
		}
		c = str.charCodeAt(++p);
	}
	if(state == 1) {
		start = p;
		state = 13;
	}
	if(state == 13) {
		if(p != start || nsubs == 0) parent.addChild(Xml.createPCData(buf.b + HxOverrides.substr(str,start,p - start)));
		return p;
	}
	throw "Unexpected end";
}
var js = {}
js.Boot = function() { }
js.Boot.__name__ = true;
js.Boot.__unhtml = function(s) {
	return s.split("&").join("&amp;").split("<").join("&lt;").split(">").join("&gt;");
}
js.Boot.__trace = function(v,i) {
	var msg = i != null?i.fileName + ":" + i.lineNumber + ": ":"";
	msg += js.Boot.__string_rec(v,"");
	if(i != null && i.customParams != null) {
		var _g = 0, _g1 = i.customParams;
		while(_g < _g1.length) {
			var v1 = _g1[_g];
			++_g;
			msg += "," + js.Boot.__string_rec(v1,"");
		}
	}
	var d;
	if(typeof(document) != "undefined" && (d = document.getElementById("haxe:trace")) != null) d.innerHTML += js.Boot.__unhtml(msg) + "<br/>"; else if(typeof(console) != "undefined" && console.log != null) console.log(msg);
}
js.Boot.__string_rec = function(o,s) {
	if(o == null) return "null";
	if(s.length >= 5) return "<...>";
	var t = typeof(o);
	if(t == "function" && (o.__name__ || o.__ename__)) t = "object";
	switch(t) {
	case "object":
		if(o instanceof Array) {
			if(o.__enum__) {
				if(o.length == 2) return o[0];
				var str = o[0] + "(";
				s += "\t";
				var _g1 = 2, _g = o.length;
				while(_g1 < _g) {
					var i = _g1++;
					if(i != 2) str += "," + js.Boot.__string_rec(o[i],s); else str += js.Boot.__string_rec(o[i],s);
				}
				return str + ")";
			}
			var l = o.length;
			var i;
			var str = "[";
			s += "\t";
			var _g = 0;
			while(_g < l) {
				var i1 = _g++;
				str += (i1 > 0?",":"") + js.Boot.__string_rec(o[i1],s);
			}
			str += "]";
			return str;
		}
		var tostr;
		try {
			tostr = o.toString;
		} catch( e ) {
			return "???";
		}
		if(tostr != null && tostr != Object.toString) {
			var s2 = o.toString();
			if(s2 != "[object Object]") return s2;
		}
		var k = null;
		var str = "{\n";
		s += "\t";
		var hasp = o.hasOwnProperty != null;
		for( var k in o ) { ;
		if(hasp && !o.hasOwnProperty(k)) {
			continue;
		}
		if(k == "prototype" || k == "__class__" || k == "__super__" || k == "__interfaces__" || k == "__properties__") {
			continue;
		}
		if(str.length != 2) str += ", \n";
		str += s + k + " : " + js.Boot.__string_rec(o[k],s);
		}
		s = s.substring(1);
		str += "\n" + s + "}";
		return str;
	case "function":
		return "<function>";
	case "string":
		return o;
	default:
		return String(o);
	}
}
js.Browser = function() { }
js.Browser.__name__ = true;
js.Browser.createXMLHttpRequest = function() {
	if(typeof XMLHttpRequest != "undefined") return new XMLHttpRequest();
	if(typeof ActiveXObject != "undefined") return new ActiveXObject("Microsoft.XMLHTTP");
	throw "Unable to create XMLHttpRequest object.";
}
var msignal = {}
msignal.Signal = function(valueClasses) {
	if(valueClasses == null) valueClasses = [];
	this.valueClasses = valueClasses;
	this.slots = msignal.SlotList.NIL;
	this.priorityBased = false;
};
msignal.Signal.__name__ = true;
msignal.Signal.prototype = {
	get_numListeners: function() {
		return this.slots.get_length();
	}
	,createSlot: function(listener,once,priority) {
		if(priority == null) priority = 0;
		if(once == null) once = false;
		return null;
	}
	,registrationPossible: function(listener,once) {
		if(!this.slots.nonEmpty) return true;
		var existingSlot = this.slots.find(listener);
		if(existingSlot == null) return true;
		if(existingSlot.once != once) throw "You cannot addOnce() then add() the same listener without removing the relationship first.";
		return false;
	}
	,registerListener: function(listener,once,priority) {
		if(priority == null) priority = 0;
		if(once == null) once = false;
		if(this.registrationPossible(listener,once)) {
			var newSlot = this.createSlot(listener,once,priority);
			if(!this.priorityBased && priority != 0) this.priorityBased = true;
			if(!this.priorityBased && priority == 0) this.slots = this.slots.prepend(newSlot); else this.slots = this.slots.insertWithPriority(newSlot);
			return newSlot;
		}
		return this.slots.find(listener);
	}
	,removeAll: function() {
		this.slots = msignal.SlotList.NIL;
	}
	,remove: function(listener) {
		var slot = this.slots.find(listener);
		if(slot == null) return null;
		this.slots = this.slots.filterNot(listener);
		return slot;
	}
	,addOnceWithPriority: function(listener,priority) {
		if(priority == null) priority = 0;
		return this.registerListener(listener,true,priority);
	}
	,addWithPriority: function(listener,priority) {
		if(priority == null) priority = 0;
		return this.registerListener(listener,false,priority);
	}
	,addOnce: function(listener) {
		return this.registerListener(listener,true);
	}
	,add: function(listener) {
		return this.registerListener(listener);
	}
}
msignal.Signal0 = function() {
	msignal.Signal.call(this);
};
msignal.Signal0.__name__ = true;
msignal.Signal0.__super__ = msignal.Signal;
msignal.Signal0.prototype = $extend(msignal.Signal.prototype,{
	createSlot: function(listener,once,priority) {
		if(priority == null) priority = 0;
		if(once == null) once = false;
		return new msignal.Slot0(this,listener,once,priority);
	}
	,dispatch: function() {
		var slotsToProcess = this.slots;
		while(slotsToProcess.nonEmpty) {
			slotsToProcess.head.execute();
			slotsToProcess = slotsToProcess.tail;
		}
	}
});
msignal.Signal1 = function(type) {
	msignal.Signal.call(this,[type]);
};
msignal.Signal1.__name__ = true;
msignal.Signal1.__super__ = msignal.Signal;
msignal.Signal1.prototype = $extend(msignal.Signal.prototype,{
	createSlot: function(listener,once,priority) {
		if(priority == null) priority = 0;
		if(once == null) once = false;
		return new msignal.Slot1(this,listener,once,priority);
	}
	,dispatch: function(value) {
		var slotsToProcess = this.slots;
		while(slotsToProcess.nonEmpty) {
			slotsToProcess.head.execute(value);
			slotsToProcess = slotsToProcess.tail;
		}
	}
});
msignal.Signal2 = function(type1,type2) {
	msignal.Signal.call(this,[type1,type2]);
};
msignal.Signal2.__name__ = true;
msignal.Signal2.__super__ = msignal.Signal;
msignal.Signal2.prototype = $extend(msignal.Signal.prototype,{
	createSlot: function(listener,once,priority) {
		if(priority == null) priority = 0;
		if(once == null) once = false;
		return new msignal.Slot2(this,listener,once,priority);
	}
	,dispatch: function(value1,value2) {
		var slotsToProcess = this.slots;
		while(slotsToProcess.nonEmpty) {
			slotsToProcess.head.execute(value1,value2);
			slotsToProcess = slotsToProcess.tail;
		}
	}
});
msignal.Slot = function(signal,listener,once,priority) {
	if(priority == null) priority = 0;
	if(once == null) once = false;
	this.signal = signal;
	this.set_listener(listener);
	this.once = once;
	this.priority = priority;
	this.enabled = true;
};
msignal.Slot.__name__ = true;
msignal.Slot.prototype = {
	set_listener: function(value) {
		if(value == null) throw "listener cannot be null";
		return this.listener = value;
	}
	,remove: function() {
		this.signal.remove(this.listener);
	}
}
msignal.Slot0 = function(signal,listener,once,priority) {
	if(priority == null) priority = 0;
	if(once == null) once = false;
	msignal.Slot.call(this,signal,listener,once,priority);
};
msignal.Slot0.__name__ = true;
msignal.Slot0.__super__ = msignal.Slot;
msignal.Slot0.prototype = $extend(msignal.Slot.prototype,{
	execute: function() {
		if(!this.enabled) return;
		if(this.once) this.remove();
		this.listener();
	}
});
msignal.Slot1 = function(signal,listener,once,priority) {
	if(priority == null) priority = 0;
	if(once == null) once = false;
	msignal.Slot.call(this,signal,listener,once,priority);
};
msignal.Slot1.__name__ = true;
msignal.Slot1.__super__ = msignal.Slot;
msignal.Slot1.prototype = $extend(msignal.Slot.prototype,{
	execute: function(value1) {
		if(!this.enabled) return;
		if(this.once) this.remove();
		if(this.param != null) value1 = this.param;
		this.listener(value1);
	}
});
msignal.Slot2 = function(signal,listener,once,priority) {
	if(priority == null) priority = 0;
	if(once == null) once = false;
	msignal.Slot.call(this,signal,listener,once,priority);
};
msignal.Slot2.__name__ = true;
msignal.Slot2.__super__ = msignal.Slot;
msignal.Slot2.prototype = $extend(msignal.Slot.prototype,{
	execute: function(value1,value2) {
		if(!this.enabled) return;
		if(this.once) this.remove();
		if(this.param1 != null) value1 = this.param1;
		if(this.param2 != null) value2 = this.param2;
		this.listener(value1,value2);
	}
});
msignal.SlotList = function(head,tail) {
	this.nonEmpty = false;
	if(head == null && tail == null) {
		if(msignal.SlotList.NIL != null) throw "Parameters head and tail are null. Use the NIL element instead.";
		this.nonEmpty = false;
	} else if(head == null) throw "Parameter head cannot be null."; else {
		this.head = head;
		this.tail = tail == null?msignal.SlotList.NIL:tail;
		this.nonEmpty = true;
	}
};
msignal.SlotList.__name__ = true;
msignal.SlotList.prototype = {
	find: function(listener) {
		if(!this.nonEmpty) return null;
		var p = this;
		while(p.nonEmpty) {
			if(Reflect.compareMethods(p.head.listener,listener)) return p.head;
			p = p.tail;
		}
		return null;
	}
	,contains: function(listener) {
		if(!this.nonEmpty) return false;
		var p = this;
		while(p.nonEmpty) {
			if(Reflect.compareMethods(p.head.listener,listener)) return true;
			p = p.tail;
		}
		return false;
	}
	,filterNot: function(listener) {
		if(!this.nonEmpty || listener == null) return this;
		if(Reflect.compareMethods(this.head.listener,listener)) return this.tail;
		var wholeClone = new msignal.SlotList(this.head);
		var subClone = wholeClone;
		var current = this.tail;
		while(current.nonEmpty) {
			if(Reflect.compareMethods(current.head.listener,listener)) {
				subClone.tail = current.tail;
				return wholeClone;
			}
			subClone = subClone.tail = new msignal.SlotList(current.head);
			current = current.tail;
		}
		return this;
	}
	,insertWithPriority: function(slot) {
		if(!this.nonEmpty) return new msignal.SlotList(slot);
		var priority = slot.priority;
		if(priority >= this.head.priority) return this.prepend(slot);
		var wholeClone = new msignal.SlotList(this.head);
		var subClone = wholeClone;
		var current = this.tail;
		while(current.nonEmpty) {
			if(priority > current.head.priority) {
				subClone.tail = current.prepend(slot);
				return wholeClone;
			}
			subClone = subClone.tail = new msignal.SlotList(current.head);
			current = current.tail;
		}
		subClone.tail = new msignal.SlotList(slot);
		return wholeClone;
	}
	,append: function(slot) {
		if(slot == null) return this;
		if(!this.nonEmpty) return new msignal.SlotList(slot);
		if(this.tail == msignal.SlotList.NIL) return new msignal.SlotList(slot).prepend(this.head);
		var wholeClone = new msignal.SlotList(this.head);
		var subClone = wholeClone;
		var current = this.tail;
		while(current.nonEmpty) {
			subClone = subClone.tail = new msignal.SlotList(current.head);
			current = current.tail;
		}
		subClone.tail = new msignal.SlotList(slot);
		return wholeClone;
	}
	,prepend: function(slot) {
		return new msignal.SlotList(slot,this);
	}
	,get_length: function() {
		if(!this.nonEmpty) return 0;
		if(this.tail == msignal.SlotList.NIL) return 1;
		var result = 0;
		var p = this;
		while(p.nonEmpty) {
			++result;
			p = p.tail;
		}
		return result;
	}
}
var $_, $fid = 0;
function $bind(o,m) { if( m == null ) return null; if( m.__id__ == null ) m.__id__ = $fid++; var f; if( o.hx__closures__ == null ) o.hx__closures__ = {}; else f = o.hx__closures__[m.__id__]; if( f == null ) { f = function(){ return f.method.apply(f.scope, arguments); }; f.scope = o; f.method = m; o.hx__closures__[m.__id__] = f; } return f; };
if(Array.prototype.indexOf) HxOverrides.remove = function(a,o) {
	var i = a.indexOf(o);
	if(i == -1) return false;
	a.splice(i,1);
	return true;
};
String.__name__ = true;
Array.__name__ = true;
Xml.Element = "element";
Xml.PCData = "pcdata";
Xml.CData = "cdata";
Xml.Comment = "comment";
Xml.DocType = "doctype";
Xml.ProcessingInstruction = "processingInstruction";
Xml.Document = "document";
msignal.SlotList.NIL = new msignal.SlotList(null,null);
com.badgerhammer.tinytextworlds.TextBlock.NORMAL = "normal";
com.badgerhammer.tinytextworlds.TextBlock.PLACE = "place";
com.badgerhammer.tinytextworlds.TextBlock.OBJECT = "object";
haxe.xml.Parser.escapes = (function($this) {
	var $r;
	var h = new haxe.ds.StringMap();
	h.set("lt","<");
	h.set("gt",">");
	h.set("amp","&");
	h.set("quot","\"");
	h.set("apos","'");
	h.set("nbsp",String.fromCharCode(160));
	$r = h;
	return $r;
}(this));
js.Browser.window = typeof window != "undefined" ? window : null;
js.Browser.document = typeof window != "undefined" ? window.document : null;
Main.main();
})();
