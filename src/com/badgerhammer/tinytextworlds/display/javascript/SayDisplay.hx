 /*  
  * Copyright (c) 2012 Nick Holder - hello@nickholder.co.uk
  * 
  * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the   * "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
  * 
  * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
  * 
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
  * 
*/

package com.badgerhammer.tinytextworlds.display.javascript;



import com.badgerhammer.tinytextworlds.actions.choice.SayChoice;
import com.badgerhammer.tinytextworlds.string.TTWStringTools;




#if haxe3
import js.Browser;
import js.html.Element;
import js.html.Event;
import js.html.Document;
#else
import js.Dom;
#end

import js.Lib;
import msignal.Signal;

/**
 * ...
 * @author Nick Holder
 */

class SayDisplay 
{
	#if haxe3
	private var _ttwSayDiv:Element;
	private var _interactiveDomElements:Array<Element>;
	#else
	private var _ttwSayDiv:HtmlDom;
	private var _interactiveDomElements:Array<HtmlDom>;
	#end
	var _document:Document;
	
	
	public var hasActions( default , null ):Bool;
	
	public var sayChoiceClicked:Signal1<Int>;
	
	public function new( ) 
	{
		#if haxe3
		_document = Browser.document;
		#else
		_document = Lib.document;
		#end
		
		sayChoiceClicked = new Signal1( );
		
		// Find the div tag to write to
		_ttwSayDiv = _document.getElementById( "TTWSay" );
		
		#if haxe3
		_interactiveDomElements = new Array<Element>();
		#else
		_interactiveDomElements = new Array<HtmlDom>();
		#end
		
		
	}
	
	
	
	// Display a new choice
	public function display( choice:SayChoice ):Void
	{
		// Clear old action display
		clear();
		
		
		
		#if haxe3
		var element:Element;
		#else
		var element:HtmlDom;
		#end
		
		// First add the query
		element = _document.createElement("span");
		element.id = "choice-query" ;
		element.innerHTML = TTWStringTools.stripCDataEnclosure( choice.text ) + "<br/>";
		// Add to HTML
		_ttwSayDiv.appendChild( element ) ;
		
		var choiceCount:Int = choice.choices.length;
		
		// Are there any actions in this sayChoice?
		hasActions = false;
		
		if ( choiceCount > 0 )
		{
			hasActions = true;
		}
		
		// iterate through actions creating elements
		for ( index in 0 ... choiceCount )
		{
			if ( index != 0 )
			{
				element = _document.createElement("br");
				_ttwSayDiv.appendChild( element ) ;
			}
			
			// Make a new element for this place/object
			element = _document.createElement("a");
			element.setAttribute( "href" , "#TTWSay");
			element.id = "choice-" + index ;
			element.className = "action interactive";
			element.innerHTML = choice.choices[index];
			
			
			
			_ttwSayDiv.appendChild( element ) ;
			
			// Has actions so add an onclick
			element.onclick = choice_onClick;
			
			// Store all elements to remove their listeners
			_interactiveDomElements.push( element );
		}
		
		
		// Jump window to TTWActions div;
		//Lib.window.location.hash =  "TTWSay";
	}
	
	
	
	// Click event handler
	private function choice_onClick( e : Event ):Void
	{	
		#if haxe3
		// Casting to Element failed in Safari so using dynamic
		var clickedElement:Dynamic = e.target;
		var choiceIndex:Int =  Std.parseInt( clickedElement.id.split("-")[1] );
		#else
		var choiceIndex:Int =  Std.parseInt( e.target.id.split("-")[1] );
		#end
		
		clear();
		sayChoiceClicked.dispatch( choiceIndex ); 
	}
	
	
	
	// Clear the div and all listeners on objects in it
	public function clear():Void
	{
		removeListeners();
		_ttwSayDiv.innerHTML = "";
	}
	
	
	
	// Remove listeners from all interactive DOM elements
	private function removeListeners():Void
	{
		while ( _interactiveDomElements.length > 0 )
		{
			_interactiveDomElements.shift().onclick = null;
		}
	}
}