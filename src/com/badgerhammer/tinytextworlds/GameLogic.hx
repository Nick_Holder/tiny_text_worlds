 /*  
  * Copyright (c) 2012 Nick Holder - hello@nickholder.co.uk
  * 
  * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the   * "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
  * 
  * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
  * 
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
  * 
*/

package com.badgerhammer.tinytextworlds;
import com.badgerhammer.tinytextworlds.actions.choice.Choice;
import com.badgerhammer.tinytextworlds.actions.choice.SayChoice;
import com.badgerhammer.tinytextworlds.actions.ActionResolver;

#if js
import com.badgerhammer.tinytextworlds.display.javascript.ActionDisplay;
import com.badgerhammer.tinytextworlds.display.javascript.Display;
#end



/**
 * @author Nick Holder
 */

class GameLogic 
{
	private var _worldXml:WorldXml;
	private var _currentPlayerLocation:String;
	private var _currentPlace:TTWPlace;
	private var _display:Display;
	private var _actionResolver:ActionResolver;
	
	
	public function new( fileName:String )
	{
		_worldXml = new WorldXml();
		_worldXml.worldLoadCompleted.add( onWorldLoadCompleted );
		_worldXml.load( fileName );
		
	}
	
	
	
	// Load complete
	private function onWorldLoadCompleted():Void
	{
		_worldXml.worldLoadCompleted.removeAll();
		
		_display = new Display(  _worldXml.getActionPromptText() ,  _worldXml.getCancelText() );
		_display.actionClicked.add( onActionClicked );
		_display.sayChoiceClicked.add( onSayChoiceClicked );
		
		_actionResolver = new ActionResolver( _worldXml  );
		_actionResolver.foundSayInActions.add( onFoundSayInActions );
		_actionResolver.placeRefreshRequested.add( onPlaceRefreshRequested );
		_actionResolver.allActionsCompleted.add( onAllActionsCompleted );
		
		// Run the game start action defined in the Xml
		_actionResolver.resolve( _worldXml.getStartUpObjectId() , _worldXml.getStartUpActionId() ); 
	}
	
	
	// Do an action called by ActionDisplay when an action is clicked
	private function onActionClicked( objectID:String , actionId:String ):Void
	{
		//_display.clear( false , false , true , false ); // Removed as I think it`s better this way
		_actionResolver.resolve( objectID , actionId );
	}
	
	

	private function onPlaceRefreshRequested():Void
	{
		// Get a placeXml
		_currentPlayerLocation =  _worldXml.getPlayerLocation() ;
		_currentPlace =  new TTWPlace( _worldXml , _currentPlayerLocation );
		
		// Get the _currentPlace'x textBlocks and pass them to the display 
		_display.displayPlace( _currentPlace.getTextBlocks() );
	}
	
	
	
	// A <say> node has been found in an action tell the display to "say" it.
	private function onFoundSayInActions( choice:SayChoice ):Void
	{
		_display.say( choice );
	}
	
	// Do a choice clicked by the user in SayDisplay
	private function onSayChoiceClicked( sayChoiceIndex:Int ):Void
	{
		_actionResolver.doSayChoice( sayChoiceIndex );
	}
	
	// Clear cetrain parts of the display.
	private function onAllActionsCompleted():Void
	{
		_display.clear( true , false , false );
	}
}